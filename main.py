from flask import Flask  
from flask import render_template
from pyfladesk import init_gui

app = Flask(__name__)

@app.route("/")
def hello():  
    return render_template('index.html')


@app.route("/home", methods=['GET'])
def home(): 
    return render_template('home.html')


if __name__ == "__main__":
    init_gui(app, port=65535)